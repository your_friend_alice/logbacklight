# logbacklight

Logarithmic backlight setter for Linux laptops. It's a setuid (it writes to `/sys/class/backlight`) so i tried to be careful (:

```
Usage: logbacklight <+|-> <FACTOR>
With +, multiply the current backlight brightness by FACTOR; with -, divide.
```

## Installation

```
make
sudo make install
```

## Example

Example `xshkdrc` (for BSPWM users)

```
{XF86MonBrightnessUp,XF86MonBrightnessDown}
	logbacklight {+,-} 1.5
```
