ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

logbacklight: logbacklight.c
	gcc -static -o logbacklight logbacklight.c

install: logbacklight
	install -m 4755 logbacklight $(DESTDIR)$(PREFIX)/bin/
