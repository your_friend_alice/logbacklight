#include <stdio.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

const char *BASE_PATH = "/sys/class/backlight/";
const int MAX_FILE_LEN = 16;

// warning this is C which is dangerous enough already and also i'm high so, look out

char *cat(const char *a, const char *b) {
    char *out;
    size_t aLen = a ? strlen(a) : 0;
    size_t bLen = b ? strlen(b) : 0;
    (out = malloc(aLen+bLen+1));
    if(out == NULL) {
        fprintf(stderr, "Error building string from components %s and %s: %s\n", a, b, strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(a) strncpy(out, a, aLen);
    if(b) strncpy(out+aLen, b, bLen);
    out[aLen+bLen] = '\0';
    return out;
}

int readInt(char *path) {
    FILE *file = fopen(path, "r");
    char raw[MAX_FILE_LEN];
    if(!file) {
        fprintf(stderr, "Error opening %s: %s\n", path, strerror(errno));
        exit(EXIT_FAILURE);
    }
    fgets(raw, MAX_FILE_LEN, file);
    fclose(file);
    return atoi(raw);
}

int getMaxBrightness(char *path) {
    char *p = cat(path, "/max_brightness");
    int val = readInt(p);
    free(p);
    return val;
}

int getBrightness(char *path) {
    char *p = cat(path, "/brightness");
    int val = readInt(p);
    free(p);
    return val;
}

void setBrightness(char *path, int val) {
    char *p = cat(path, "/brightness");
    FILE *file = fopen(p, "w");
    free(p);
    if(!file) {
        fprintf(stderr, "Error opening %s: %s\n", p, strerror(errno));
        exit(EXIT_FAILURE);
    }
    fprintf(file, "%d", val);
    fclose(file);
}

void usage(const char *bin) {
    printf("Usage: %s <+|-> <FACTOR>\nWith +, multiply the current backlight brightness by FACTOR; with -, divide.\n", bin);
    exit(EXIT_FAILURE);
}

void changeBrightness(char *path, double factor) {
    double old = (double)getBrightness(path);
    double max = (double)getMaxBrightness(path);
    double new = old * factor;
    if((int)new == (int)old) {
        if(factor>=1) {
            new++;
        } else {
            new--;
        }
    }
    if(new < 1) {
        new = 1;
    } else if(new > max) {
        new = max;
    }
    printf("%s: %d / %d\n", path, (int)new, (int)old);
    setBrightness(path, (int)new);
}

int main(int argc, const char *argv[]) {
    if(argc != 3 || strlen(argv[1]) != 1) {
        usage(argv[0]);
    }
    double factor = atof(argv[2]);

    switch(argv[1][0]) {
        case '+':
            break;
        case '-':
            factor = 1/factor;
            break;
        default:
            usage(argv[0]);
    }

    DIR *dir = opendir(BASE_PATH);
    // open the dir
    if(!dir) {
        fprintf(stderr, "Error opening %s: %s\n", BASE_PATH, strerror(errno));
        exit(EXIT_FAILURE);
    }
    struct dirent *entry;
    while((entry = readdir(dir)) != NULL) {
        if(entry->d_name[0] != '.') {
            char *p = cat(BASE_PATH, entry->d_name);
            changeBrightness(p, factor);
            free(p);
        }
    }
}
